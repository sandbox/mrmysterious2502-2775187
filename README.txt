INTRODUCTION
------------

Displays users logged into a Minecraft server. Capable of using the Minecraft
ping protocol as well as the Gamespy4 protocol.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/minecraft_players

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/minecraft_players

REQUIREMENTS
------------

This module requires the following modules:

 * Minecraft Query (https://drupal.org/project/minecraft_query)

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 
 * On the "Blocks" page, enable the "Minecraft Status" block and then click 
   configure:

   - Server IP: The Minecraft server's ipv4 address (or domain, i.e. 
     mc.dimension-seven.net).

   - Server Port: The port that the Minecraft server is listening on.

   - Server Protocol: The "language" that the server speaks. If you don't know, 
     choose "Minecraft Ping".
     
   - Image URL: The URL that the player names will be appended to when fetching
     a player image.
