<div class="minecraft-players">
  <ul>
    <?php if(!empty($variables['minecraft_players']['players'])):?>
      <?php foreach($variables['minecraft_players']['players'] as $player):?>
        <li class="minecraft-player">
          <img class="minecraft-player-image" src="<?php print $variables['minecraft_players']['image_url'] . $player;?>"></img>
          <div class="minecraft-player-name">
            <?php print $player;?>
          </div>
        </li>
      <?php endforeach;?>
    <?php else:					
      print t("There are currently no players online.");					
    endif;?>
  </ul>
</div>